﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	public static GameController instance;

	public static readonly int CONTROL_TOUCH = 1;
	public static readonly int CONTROL_ACCELEROMETER = 2;

	public static readonly float DEFAULT_PLATFORM_SPEED = 2f;
	public static readonly float DEFAULT_PLATFORM_SPAWN_RATE = 2f;
	public static readonly float DEFAULT_SPEEDUP_INTERVAL = 1f;
	public static readonly float DEFAULT_SPEEDUP_AMOUNT = 0.1f;

	public static readonly float MAX_PLATFORM_SPEED = 5f;
	public static readonly float MIN_PLATFORM_SPAWN_RATE = 0.3f;

	private int gameMode;
	private bool gameRunning;
	private float score;
	private float lastSpeedUp = 0f;

	private float platformSpeed;
	private float platformSpawnRate;
	private float speedUpAmount;
	private float speedUpInterval;

	// Use this for initialization
	void Awake ()
	{
		DontDestroyOnLoad (this);
		if (instance == null) {
			instance = this;
		}

		instance.gameMode = gameMode;
		instance.gameRunning = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (gameRunning) {
			score += Time.deltaTime;

			lastSpeedUp += Time.deltaTime;
			if (lastSpeedUp >= speedUpInterval) {
				SpeedUp ();
			}
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (gameRunning) {
				if (Input.GetKeyDown (KeyCode.Escape))
					StopGame ();
			} else {
				StopGame ();
			}
		}
	}

	public void SetGameMode (int mode)
	{
		gameMode = mode;
		StartGame ();
	}

	void SpeedUp ()
	{
		if (platformSpeed + speedUpAmount < MAX_PLATFORM_SPEED)
			platformSpeed += speedUpAmount;

		if (platformSpawnRate - speedUpAmount / 2 > MIN_PLATFORM_SPAWN_RATE)
			platformSpawnRate -= speedUpAmount / 2;
		
		lastSpeedUp = 0;
	}

	public int GetGameMode ()
	{
		return gameMode;
	}

	public float GetPlatformSpeed ()
	{
		return platformSpeed;
	}

	public float GetPlatformSpawnRate ()
	{	
		return platformSpawnRate;
	}

	public bool IsGameRunning ()
	{
		return gameRunning;
	}

	public void StartGame ()
	{
		platformSpeed = DEFAULT_PLATFORM_SPEED;
		platformSpawnRate = DEFAULT_PLATFORM_SPAWN_RATE;
		speedUpAmount = DEFAULT_SPEEDUP_AMOUNT;
		speedUpInterval = DEFAULT_SPEEDUP_INTERVAL;

		lastSpeedUp = 0f;
		score = 0f;

		gameRunning = true;
	}

	public void StopGame ()
	{
		gameRunning = false;
		SceneManager.LoadScene ("Menu");
	}

	public void EndGame ()
	{
		gameRunning = false;

		GameOver.UpdateFinalScore (score);
		GameOver.UpdateGameScene (SceneManager.GetActiveScene ().name);
		SceneManager.LoadScene ("GameOver");
	}
}
