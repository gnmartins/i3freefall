﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{

	private Rigidbody2D rb2d;

	// Use this for initialization
	void Start ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
		rb2d.velocity = new Vector2 (0, GameController.instance.GetPlatformSpeed ());
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (!GameController.instance.IsGameRunning ()) {
			rb2d.velocity = Vector2.zero;
		} else {
			rb2d.velocity = new Vector2 (0, GameController.instance.GetPlatformSpeed ());
		}
	}
}
