﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayGameSystem : MonoBehaviour
{

	public Toggle isTouch;
	public Toggle isGesture;

	void Start ()
	{
		int mode = PlayerPrefs.GetInt ("mode");
		if (mode == GameController.CONTROL_TOUCH)
			isTouch.isOn = true;
		else
			isGesture.isOn = true;
	}

	public void PlayGame ()
	{
		SaveGameMode ();
		SceneManager.LoadScene ("Game");
		if (isGesture.isOn) {
			GameController.instance.SetGameMode (GameController.CONTROL_ACCELEROMETER);
		} else {
			GameController.instance.SetGameMode (GameController.CONTROL_TOUCH);
		}
	}

	public void ViewStats ()
	{
		SaveGameMode ();
		SceneManager.LoadScene ("Stats");
	}

	public void QuitGame ()
	{
		Application.Quit ();
	}

	void SaveGameMode ()
	{
		if (isGesture.isOn) {
			PlayerPrefs.SetInt ("mode", GameController.CONTROL_ACCELEROMETER);
		} else {
			PlayerPrefs.SetInt ("mode", GameController.CONTROL_TOUCH);
		}
	}
}
