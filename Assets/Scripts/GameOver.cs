﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{

	public static Text scoreText;
	public static Text restartText;
	public static Text hsText;
	public static string gameScene;
	public static int finalScore;

	private float timePassed = 0f;

	// Use this for initialization
	void Start ()
	{
		bool newHs = UpdateHighScore ();

		scoreText = GameObject.Find ("Score").GetComponent<Text> ();
		restartText = GameObject.Find ("TapToRestart").GetComponent<Text> ();
		hsText = GameObject.Find ("Highscore").GetComponent<Text> ();

		scoreText.text = "Score : " + finalScore; 

		if (!newHs)
			hsText.text = "";

		timePassed = 0f;
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		timePassed += Time.deltaTime;
		if (Input.GetMouseButtonDown (0) && timePassed > 0.5f) {	
			GameController.instance.StartGame ();
			SceneManager.LoadScene (gameScene);
		}

		//BlinkText (restartText, 2.0f);
		BlinkTextSize (hsText, 3f, 18);
	}

	public static void UpdateFinalScore (float score)
	{
		finalScore = (int)score;
	}

	public static void UpdateGameScene (string scene)
	{
		gameScene = scene;
	}

	bool UpdateHighScore ()
	{
		int prev_hs;
		int mode = PlayerPrefs.GetInt ("mode");
		if (mode == GameController.CONTROL_ACCELEROMETER) {
			prev_hs = PlayerPrefs.GetInt ("hsGesture");
			if (finalScore > prev_hs) {
				PlayerPrefs.SetInt ("hsGesture", finalScore);
				return true;
			}
		} else {
			prev_hs = PlayerPrefs.GetInt ("hsTouch");
			if (finalScore > prev_hs) {
				PlayerPrefs.SetInt ("hsTouch", finalScore);
				return true;
			}
		}
		return false;
	}

	void BlinkTextAlpha (Text t, float speed)
	{
		float alpha = Mathf.Clamp (Mathf.Sin (Time.time * speed), 0, 1);
		t.color = new Color (t.color.r, t.color.g, t.color.b, alpha);
	}

	void BlinkTextSize (Text t, float speed, int maxSize)
	{
		float alpha = Mathf.Sin (Time.time * speed) + 2;
		t.fontSize = (int) (maxSize / 3 * alpha);
	}

	
}
