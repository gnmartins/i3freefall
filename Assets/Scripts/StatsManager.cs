﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatsManager : MonoBehaviour {

	private Text hsGestureText;
	private Text hsTouchText;
	private Text resetButtonText;

	private bool reset = false;
	private float resetTimer = 0f;

	// Use this for initialization
	void Start () 
	{
		hsGestureText = GameObject.Find ("GestureScore").GetComponent<Text> ();
		hsTouchText = GameObject.Find ("TouchScore").GetComponent<Text> ();
		resetButtonText = GameObject.Find ("ResetLabel").GetComponent<Text> ();

		SetScores ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (reset) {
			resetTimer += Time.deltaTime;
		}
		if (resetTimer > 3.0f) {
			reset = false;	
			resetButtonText.text = "Reset Stats";
			resetTimer = 0f;
		}
	}

	void SetScores () 
	{
		int hsGesture = PlayerPrefs.GetInt ("hsGesture");
		int hsTouch = PlayerPrefs.GetInt ("hsTouch");

		hsGestureText.text = hsGesture.ToString ();
		hsTouchText.text = hsTouch.ToString ();

	}

	public void BackToMenu ()
	{
		SceneManager.LoadScene ("Menu");
	}

	public void ResetScores ()
	{
		if (reset) {
			PlayerPrefs.SetInt ("hsGesture", 0);
			PlayerPrefs.SetInt ("hsTouch", 0);
			resetButtonText.text = "Stats Reset!";
			resetTimer = 0f;
			SetScores ();
		} else {
			resetButtonText.text = "Are you sure?";
			reset = true;
			resetTimer = 0f;
		}
	}
}
