﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPool : MonoBehaviour
{
	// GameObjects
	public GameObject platformModel;
	public GameObject movingPlatforms;

	// Platform settings
	private GameObject[] platforms;
	private Vector2 platformPoolPosition = new Vector2 (-10f, -10f);

	// Spawning settings
	private float ySpawnPosition = -10f;
	private float xMinPosition = -3f;
	private float xMaxPosition = 3f;
	private float platformHeight = 1f;
	private float platformMinWidth = 0.7f;
	private float platformMaxWidth = 1.1f;
	private float timeSinceLastSpawn;
	private int maxSpawnedPlatforms = 15;
	private int currentPlatform = 0;

	// Use this for initialization
	void Start ()
	{
		InstantiatePlatforms ();
		timeSinceLastSpawn = GameController.instance.GetPlatformSpawnRate ();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		timeSinceLastSpawn += Time.deltaTime;

		if (GameController.instance.IsGameRunning ())
		if (timeSinceLastSpawn >= GameController.instance.GetPlatformSpawnRate ()) {
			SpawnPlatform ();
			timeSinceLastSpawn = 0f;
		}
	}

	void InstantiatePlatforms ()
	{
		platforms = new GameObject[maxSpawnedPlatforms];
		for (int i = 0; i < maxSpawnedPlatforms; i++) {
			platforms [i] = (GameObject) Instantiate (platformModel, platformPoolPosition, Quaternion.identity);
			platforms [i].transform.parent = movingPlatforms.transform;
		}
	}

	void SpawnPlatform ()
	{
		float xSpawnPosition = Random.Range (xMinPosition, xMaxPosition);
		float platformWidth = Random.Range (platformMinWidth, platformMaxWidth);

		platforms [currentPlatform].transform.position = new Vector2 (xSpawnPosition, ySpawnPosition);
		platforms [currentPlatform].transform.localScale = new Vector3 (platformWidth, platformHeight, 1);

		// TODO: distance between platform2 and platform1 must be big enough so
		// that the ball can go between them.


		// iterating over spawned obstacles
		currentPlatform++;
		if (currentPlatform >= maxSpawnedPlatforms) {
			currentPlatform = 0;
		}
	}
}
