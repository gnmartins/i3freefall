using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{

	public float maxSpeed = 5f;
	public float XLIMIT = 2.7f;
	public float YLIMIT = 5.2f;

	public static readonly float MAX_ACCELERATION = 1.5f;

	private Rigidbody2D rb2d;

	private int controlMode;

	// Touch control variables
	private float targetX = 0f;
	private float acceleration = 0f;
	private float movingRight = 0f;

	// Accelerometer control variables
	private float smooth = 0.4f;
	private float sensitivity = 4f;
	private Vector3 initialAcceleration;
	private Vector3 currentAcceleration;

	void Start ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
		controlMode = GameController.instance.GetGameMode ();

		if (controlMode == GameController.CONTROL_ACCELEROMETER) {
			initialAcceleration = Input.acceleration;
			currentAcceleration = Vector3.zero;
		} 

		SetStartX ();
	}

	void FixedUpdate ()
	{
		KeepInBounds ();

		if (controlMode == GameController.CONTROL_ACCELEROMETER)
			AccelerateTowardsDirection ();
		else
			MoveTowardsTarget ();

	}

	void OnCollisionExit2D (Collision2D coll) 
	{
		if (coll.gameObject.name != "Lower Limit") {
			if (rb2d.velocity.y > 0) {
				rb2d.velocity = new Vector2 (rb2d.velocity.x, 0);
			}
		}
	}

	void SetStartX ()
	{
		rb2d.position = new Vector2 (Random.Range (-XLIMIT, XLIMIT), rb2d.position.y);
	}

	void KeepInBounds ()
	{
		// Mantaining object in screen limits
		if (rb2d.position.x > XLIMIT) {
			rb2d.position = new Vector2 (XLIMIT, rb2d.position.y);
		} else if (rb2d.position.x < -XLIMIT) {
			rb2d.position = new Vector2 (-XLIMIT, rb2d.position.y);
		}

		// Checking for game end
		if (rb2d.position.y > YLIMIT) {
			GameController.instance.EndGame ();
		}
	}

	/* ====================== TOUCH =============================================================================== */
	void MoveTowardsTarget ()
	{
		GetTargetX ();
		if (!CloseEnoughToTarget ()) {
			// Making the object move towards target with increasing speed
			movingRight = rb2d.position.x < targetX ? 1f : -1f; // acceleration > 0 if going right
			if (acceleration < MAX_ACCELERATION)
				acceleration += 0.1f;
			rb2d.velocity = new Vector2 (acceleration * movingRight * maxSpeed, rb2d.velocity.y);
		} else {
			// Stopping the object's acceleration
			acceleration = 0.0f;
			rb2d.velocity = new Vector2 (0, rb2d.velocity.y);
		}
	}

	void GetTargetX ()
	{
		if (Input.GetMouseButton (0)) {
			Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
			targetX = MapTouchX (mousePos.x);
		}
	}

	bool CloseEnoughToTarget ()
	{
		float currentX = rb2d.position.x;
		if (currentX >= targetX - (MAX_ACCELERATION * 0.1f) && currentX <= targetX + (MAX_ACCELERATION * 0.1f)) {
			return true;
		}
		return false;
	}

	float MapTouchX (float x)
	{
		return MapRange (x, 0, 1, - XLIMIT - 0.2f, XLIMIT + 0.2f);
	}
	/* ============================================================================================================ */

	/* ====================== ACCELEROMETER ======================================================================= */
	void AccelerateTowardsDirection ()
	{
		currentAcceleration = Vector3.Lerp (currentAcceleration, Input.acceleration - initialAcceleration, smooth);

		// Clamping acceleration for fairness
		acceleration = Mathf.Clamp (currentAcceleration.x * sensitivity, -MAX_ACCELERATION, MAX_ACCELERATION); 
		rb2d.velocity = new Vector2 (acceleration * maxSpeed, rb2d.velocity.y);
	}
	/* ============================================================================================================ */

	Vector2 GetPosition ()
	{
		return rb2d.position;
	}

	float MapRange (float x, float prevMin, float prevMax, float currMin, float currMax)
	{
		return currMin + (x - prevMin) * (currMax - currMin) / (prevMax - prevMin);
	}
}
